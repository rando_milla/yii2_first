<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Items */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="items-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
	<?php
	if($model->tags)
	{
		$wholetag='';
			foreach($model->tags as $tag)
			{
				$wholetag.=$tag->name.",";
			}
			$model->text1=substr($wholetag, 0, -1);
	}
	?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'item_name',
            'vendor',
            'type',
            'serial_number',
            'price',
            'weight',
            'color',
            'release_date',
			[
			'attribute'=>'tags',
			'value'=>$model->text1,
			],
			[
			'attribute'=>'photo',
			'value'=>$model->photo,
			'format'=>['image',['width'=>'100']],
			],
            'created_time',
        ],
    ]) ?>

</div>
