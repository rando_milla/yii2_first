<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Vendor;
use app\models\Type;
use app\models\Tags;
/* @var $this yii\web\View */
/* @var $model app\models\Items */
/* @var $form yii\widgets\ActiveForm */
?>
<?php 
//var_dump($model);
//exit();
?>
<div class="items-form">
		<?php
	if($model->tags)
	{
		
			foreach($model->tags as $tag)
			{
				$text1[]=$tag->name;
			}
		$model->text1=$text1;
	}
	?>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'item_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendor')->dropDownList(ArrayHelper::map(Vendor::find()->all(),'id','name')) ?>

    <?= $form->field($model, 'type')->dropDownList(ArrayHelper::map(Type::find()->all(),'id','name')) ?>

    <?= $form->field($model, 'serial_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'release_date')->widget(\yii\jui\DatePicker::classname(),['language' => 'en']) ?>

    <?= $form->field($model, 'file')->fileInput()->label("Photo") ?>
	
	<?= $form->field($model, 'text1')->checkboxList(ArrayHelper::map(Tags::find()->all(),'name','name'),['multiple' => true])->label('Tags'); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
