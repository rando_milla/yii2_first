<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\SidebarWidget;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="items-index">
	<div class="col-lg-10">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Items', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'item_name',
            'vendor',
            'type',
            'serial_number',
            'price',
            'weight',
            'color',
            'release_date',
			[
			'attribute'=>'tags',
			'value'=>function($data) { 
			$wholetag='';
			foreach($data['tags'] as $tag)
			{
				$wholetag.=$tag->name.",";
			}
			return substr($wholetag, 0, -1); },
			],
            [
			'attribute'=>'photo',
			'value'=>function($data){ return $data['photo']; },
			'format'=>['image',['width'=>'100']],
			],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>
<div class="col-lg-2">
<h3>3 Latest Items</h3>
<?=SidebarWidget::widget(['items'=>$latestitems]); ?>
</div>
</div>
