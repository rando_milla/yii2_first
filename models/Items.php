<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use dosamigos\taggable\Taggable;
use app\models\Tags;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%items}}".
 *
 * @property integer $id
 * @property string $item_name
 * @property integer $vendor
 * @property integer $type
 * @property string $serial_number
 * @property double $price
 * @property string $weight
 * @property string $color
 * @property string $release_date
 * @property string $photo
 * @property string $created_time
 * @property string $text1
 *
 * @property Type $type0
 * @property Vendor $vendor0
 * @property file $file
 */
class Items extends \yii\db\ActiveRecord
{
	
public $file;
public $text1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%items}}';
    }
	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		        return [
            [
                'class' => Taggable::className(),
            ],
        ];
	}
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'vendor','type', 'serial_number', 'price', 'weight', 'color', 'release_date','text1'], 'required'],
            [['vendor', 'type'], 'integer'],
            [['price'], 'number'],
            [['created_time'], 'safe'],
            [['item_name', 'serial_number', 'weight', 'color', 'photo'], 'string', 'max' => 255],
            [['type'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['type' => 'id']],
            [['vendor'], 'exist', 'skipOnError' => true, 'targetClass' => Vendor::className(), 'targetAttribute' => ['vendor' => 'id']],
			[['file'],'file'],
			[['release_date'],'date','format'=>'php:Y-m-d'],
			[['tagNames'],'safe'],
			
        ];
    }
	/**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_name' => 'Item Name',
            'vendor' => 'Vendor',
            'type' => 'Type',
            'serial_number' => 'Serial Number',
            'price' => 'Price',
            'weight' => 'Weight',
            'color' => 'Color',
            'release_date' => 'Release Date',
            'photo' => 'Photo',
            'created_time' => 'Created Time',
			'text1'=>'Tags',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type']);
    }
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTags()
	{
		return $this->hasMany(Tags::className(), ['id' => 'tag_id'])->viaTable('items_tag_assn', ['item_id' => 'id']);
	}
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorr()
    {
        return $this->hasOne(Vendor::className(), ['id' => 'vendor']);
    }
}
