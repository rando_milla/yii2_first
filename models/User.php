<?php

namespace app\models;

use Yii;
use yii\helpers\Security;
use yii\web\IdentityInterface;
use yii\helpers\BaseFileHelper;
use yii\db\Query;
use yii\db\Transaction;
use yii\base\Exception;


//use app\models\UserPrivacy;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username
 * @property string $password
 * @property string $status
 *
 */
class User extends \yii\db\ActiveRecord  implements IdentityInterface
{    
	/**
     * @inheritdoc
     */
	 const ROLE_USER = 1;
	const ROLE_ADMIN = 3;
	const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username','password'], 'required'],
            [["status"], 'safe'],
            [['password'], 'string'],
            [['username'], 'string', 'max' => 255],
        ];
    }
    
    public function usernameValidation()
    {
        if($this->id && $this->id > 0)
        {
            
            $user = User::find()->where(["username" => $this->username])->one();
            
            if($user instanceof User)
            {
                $user2=User::find()->where(["username" => $this->username])->andwhere(['<>','id',$this->id])->one();
               if($user2 instanceof User)
                 {
                   $this->addError("username", "Username already exists"); 
                 } 
            }
        }
        else
        {
             $user = User::find()->where(["username" => $this->username])->one();
             
             if($user instanceof User)
             {
                $this->addError("username", "Username already exists1"); 
             }
        }
    }
	
	public function checkUsername()
	{
		$user = User::find()->where(["username" => $this->username])->one();
		
		if($user instanceof User)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
		
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' =>'ID',
            'username' => 'Username',
            'password' => 'Password',
        	'status' =>'Status',           
        ];
    }
    
	
	public static function getUserByUsername($username)
	{
		return  User::find()->where(['username' => $username])->andWhere(['status'=>1])->one();
        
       
	}



	public function validatePassword($password)
	{
	   
		if(md5($password) == $this->password)
        {
                return true;   
        }	
		return false;
	}
		
	  /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
	 /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }
	/**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
       // return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        //return $this->getAuthKey() === $authKey;
    }
	
}
