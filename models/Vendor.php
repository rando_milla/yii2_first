<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%vendor}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $logo
 *
 * @property Items[] $items
 */
class Vendor extends \yii\db\ActiveRecord
{
	public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vendor}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'logo'], 'string', 'max' => 255],
			['file','file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'logo' => 'Logo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Items::className(), ['vendor' => 'id']);
    }
}
