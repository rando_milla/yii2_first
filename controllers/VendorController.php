<?php

namespace app\controllers;

use Yii;
use app\models\Vendor;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use app\components\AccessRule;
use app\models\User;
/**
 * VendorController implements the CRUD actions for Vendor model.
 */
class VendorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
			'access' => [
					'class' => AccessControl::className(),
					// We will override the default rule config with the new AccessRule class
					'ruleConfig' => [
						'class' => AccessRule::className(),
					],
					'only' => ['create', 'update', 'delete'],
					'rules' => [
						[
							'actions' => ['create'],
							'allow' => true,
							// Allow users, moderators and admins to create
							'roles' => [
								User::ROLE_USER,
								User::ROLE_ADMIN
							],
						],
						[
							'actions' => ['update'],
							'allow' => true,
							// Allow moderators and admins to update
							'roles' => [
								User::ROLE_USER,
								User::ROLE_ADMIN
							],
						],
						[
							'actions' => ['delete'],
							'allow' => true,
							// Allow admins to delete
							'roles' => [
								User::ROLE_ADMIN
							],
						],
					],
				],
        ];
    }

    /**
     * Lists all Vendor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Vendor::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vendor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vendor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vendor();

        if ($model->load(Yii::$app->request->post())) {
			$model->file=UploadedFile::getInstance($model,'file');
			if($model->validate())
			{
				if(isset($model->file))
				{
				if($model->file->saveAs('upload/vendor/'.$model->file->baseName.'.'.$model->file->extension))
				{
					
					$model->logo='upload/vendor/'.$model->file->baseName.'.'.$model->file->extension;
					if($model->save())
					{

						return $this->redirect(['view', 'id' => $model->id]);
					}
				}
				}
				else
				{
					$model->logo='upload/default-thumbnail.jpg';
					if($model->save())
					{

						return $this->redirect(['view', 'id' => $model->id]);
					}
				}
			}
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Vendor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
        if ($model->load(Yii::$app->request->post())) {
			{
				$model->file=UploadedFile::getInstance($model,'file');
				if(isset($model->file))
				{
					if($model->file->saveAs('upload/vendor/'.$model->file->baseName.'.'.$model->file->extension) && unlink(getcwd().'/'.$model->logo))
					{
						$model->logo='upload/vendor/'.$model->file->baseName.'.'.$model->file->extension;
						if($model->save())
						{
							return $this->redirect(['view','id'=>$model->id]);
						}
					}
				}
				else
				{
					if($model->save())
						{
							return $this->redirect(['view','id'=>$model->id]);
						}
				}
			}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Vendor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
		if($model->logo!='upload/default-thumbnail.jpg')
		{
		if(unlink(getcwd().'/'.$model->logo))
		{
			if($model->delete())
			{
				return $this->redirect(['index']);
			}
		}
		}
		else
		{
			if($model->delete())
			{
				return $this->redirect(['index']);
			}
		}
    }

    /**
     * Finds the Vendor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vendor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vendor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
