<?php

namespace app\controllers;

use Yii;
use app\models\Items;
use app\models\ItemsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use app\components\AccessRule;
use app\models\User;
/**
 * ItemsController implements the CRUD actions for Items model.
 */
class ItemsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
			
            'access' => [
					'class' => AccessControl::className(),
					// We will override the default rule config with the new AccessRule class
					'ruleConfig' => [
						'class' => AccessRule::className(),
					],
					'only' => ['create', 'update', 'delete'],
					'rules' => [
						[
							'actions' => ['create'],
							'allow' => true,
							// Allow users, moderators and admins to create
							'roles' => [
								User::ROLE_USER,
								User::ROLE_ADMIN
							],
						],
						[
							'actions' => ['update'],
							'allow' => true,
							// Allow moderators and admins to update
							'roles' => [
								User::ROLE_USER,
								User::ROLE_ADMIN
							],
						],
						[
							'actions' => ['delete'],
							'allow' => true,
							// Allow admins to delete
							'roles' => [
								User::ROLE_ADMIN
							],
						],
					],
				],
			];
        
    }
    /**
     * Lists all Items models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$latestitems= Items::find()->orderBy(['id' => SORT_DESC])->limit(3)->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'latestitems'=>$latestitems,
        ]);
    }

    /**
     * Displays a single Items model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Items model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Items();

        if ($model->load(Yii::$app->request->post())) {
			
			$model->file=UploadedFile::getInstance($model,'file');
			$model->release_date=\Yii::$app->formatter->asDatetime($model->release_date,'php:Y-m-d');
			
			if($model->validate())
			{
				
				if(isset($model->file))
				{
				if($model->file->saveAs('upload/'.$model->file->baseName.'.'.$model->file->extension))
				{
					
					$model->photo='upload/'.$model->file->baseName.'.'.$model->file->extension;
					$model->created_time=time();
					$model->tagNames=$model->text1;
					
					if($model->save())
					{

						return $this->redirect(['view', 'id' => $model->id]);
					}
				}
				}
				else
				{
					$model->photo='upload/default-thumbnail.jpg';
					$model->created_time=time();
					$model->tagNames=$model->text1;
					
					if($model->save())
					{

						return $this->redirect(['view', 'id' => $model->id]);
					}
				}
			}
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Items model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
			
			$model->file=UploadedFile::getInstance($model,'file');
			$model->release_date=\Yii::$app->formatter->asDatetime($model->release_date,'php:Y-m-d');
			if($model->validate())
			{
				if(isset($model->file))
				{
				if($model->file->saveAs('upload/'.$model->file->baseName.'.'.$model->file->extension) && unlink(getcwd().'/'.$model->photo))
				{
					$model->photo='upload/'.$model->file->baseName.'.'.$model->file->extension;
					$model->tagNames=$model->text1;
					if($model->save())
					{
						return $this->redirect(['view', 'id' => $model->id]);
					}
				}
				}
				else
				{
					$model->tagNames=$model->text1;
					if($model->save())
					{
						return $this->redirect(['view','id'=>$model->id]);
					}
				}
			}
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Items model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		
        $model=$this->findModel($id);
		if($model->photo!='upload/default-thumbnail.jpg')
		{
		if(unlink(getcwd().'/'.$model->photo))
		{
			if($model->delete())
			{
				 return $this->redirect(['index']);
			}
		}
		}
		else
		{
			if($model->delete())
			{
				 return $this->redirect(['index']);
			}
		}
    }

    /**
     * Finds the Items model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Items the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Items::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
